import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SelectIllnessComponent} from './components/select-illness/select-illness.component';
import {PatientsComponent} from './components/patients/patients.component';
import {HospitalsComponent} from './components/hospitals/hospitals.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: SelectIllnessComponent},
  {path: 'suggested-hospitals/:painLevel', component: HospitalsComponent},
  {path: 'patient-list', component: PatientsComponent},
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
