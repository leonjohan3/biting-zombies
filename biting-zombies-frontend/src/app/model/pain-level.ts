export interface PainLevel {
  level: number;
  description: string;
  image?: string;
}
