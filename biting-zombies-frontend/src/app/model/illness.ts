// @ModelConfig({
//   type: 'Illness',
//   endpoint: 'illnesses'
// })
export interface Illness {
  id: number;
  name: string;
}
