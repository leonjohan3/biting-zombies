import {HalModel, ModelConfig} from 'ngx-hal';

@ModelConfig({
  type: 'HospitalCollection',
  endpoint: 'hospitals'
})
export class HospitalCollection extends HalModel {
}
