import {WaitingList} from './waiting-list';

export interface Hospital {
  id: number;
  name: string;
  waitingList: WaitingList[];
}
