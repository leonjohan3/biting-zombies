import {Resource} from '@lagoshny/ngx-hal-client';

export class Patient extends Resource {
  public firstName: string;
  public lastName: string;
  public birthDate: Date;
  public illnessId?: number;
  public painLevel: number;
  public illness?: string;
}
