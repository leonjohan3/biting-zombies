import {HalModel, ModelConfig} from 'ngx-hal';

@ModelConfig({
  type: 'IllnessCollection',
  endpoint: 'illnesses'
})
export class IllnessCollection extends HalModel {
}
