import {Injectable} from '@angular/core';
import {ExternalConfiguration, ExternalConfigurationHandlerInterface} from '@lagoshny/ngx-hal-client';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExternalConfigurationService implements ExternalConfigurationHandlerInterface {

  constructor(private http: HttpClient) {
  }

  getExternalConfiguration(): ExternalConfiguration {
    return {};
  }

  getHttp(): HttpClient {
    return this.http;
  }

  getProxyUri(): string {
    return '';
  }

  getRootUri(): string {
    return '/api';
  }

  setExternalConfiguration(externalConfiguration: ExternalConfiguration): any {
  }

  deserialize(): any {
  }

  serialize(): any {
  }
}
