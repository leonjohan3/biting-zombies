import {Injectable} from '@angular/core';
import {DatastoreService, HalDocument, ModelService} from 'ngx-hal';
import {HospitalCollection} from '../model/hospital-collection';
import {Hospital} from '../model/hospital';

@Injectable({
  providedIn: 'root'
})
export class HospitalService extends ModelService<HospitalCollection> {
  private hospitals: Hospital[];

  constructor(datastore: DatastoreService) {
    super(datastore, HospitalCollection);
  }

  private getNextPageHospitals(hospitals: Hospital[], url?: string): void {

    this.find({}, true, undefined, undefined,
      undefined, url).subscribe(
      (halDocument: HalDocument<HospitalCollection>) => {
        halDocument.models.forEach(item => {
          const resource = (item as any).resource;
          hospitals.push({id: resource.id, name: resource.name, waitingList: resource.waitingList});
        });
        const links = (halDocument as any).rawResource._links;

        if (links.next) {
          this.getNextPageHospitals(this.hospitals, links.next.href);
        }
      });
  }

  getAllHospitals() {
    if (!this.hospitals) {
      this.hospitals = [];
      this.getNextPageHospitals(this.hospitals);
    }
    return this.hospitals;
  }
}
