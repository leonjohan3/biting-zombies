import {Injectable, Injector} from '@angular/core';
import {Patient} from '../model/patient';
import {RestService} from '@lagoshny/ngx-hal-client';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PatientService extends RestService<Patient> {

  constructor(injector: Injector) {
    super(Patient, 'patients', injector);
  }

  public findByFirstNameIgnoreCaseAndLastNameIgnoreCase(firstName: string, lastName: string): Observable<Patient[]> {
    const options: any = {params: [{key: 'firstName', value: firstName}, {key: 'lastName', value: lastName}]};
    return this.search('findByFirstNameIgnoreCaseAndLastNameIgnoreCase', options);
  }
}
