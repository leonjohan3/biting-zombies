import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {PainLevel} from '../model/pain-level';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PainLevelService {
  private apiUrl = 'api/painLevels';

  constructor(private http: HttpClient) {
  }

  getPainLevels(): Observable<PainLevel[]> {
    return this.http.get<PainLevel[]>(this.apiUrl)
      .pipe(
        map(collection => {
          const painLevels: PainLevel[] = [];

          collection.forEach(item => {

            let image: string;

            switch (item.level) {
              case 1:
                image = 'pain-mild.png';
                break;
              case 2:
                image = 'pain-moderate.png';
                break;
              case 3:
                image = 'pain-severe.png';
                break;
              case 4:
                image = 'pain-extreme.png';
                break;
              default:
                image = 'pain-none.png';
            }
            painLevels.push({level: item.level, description: item.description, image});
          });
          return painLevels;
        }),
        catchError(this.handleError<PainLevel[]>('getPainLevels', []))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error('handleError', error);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
