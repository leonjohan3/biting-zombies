import {DatastoreConfig, DatastoreService} from 'ngx-hal';
import {HttpClient} from '@angular/common/http';

@DatastoreConfig({
  network: {baseUrl: '/dmmw-api'}
})
export class HalDatastoreService extends DatastoreService {

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }
}
