import {Injectable} from '@angular/core';
import {DatastoreService, HalDocument, ModelService} from 'ngx-hal';
import {IllnessCollection} from '../model/illness-collection';
import {Illness} from '../model/illness';

@Injectable({
  providedIn: 'root'
})
export class IllnessService extends ModelService<IllnessCollection> {
  private illnesses: Illness[];

  constructor(datastore: DatastoreService) {
    super(datastore, IllnessCollection);
  }

  private getNextPageIllnesses(illnesses: Illness[], url?: string): void {

    this.find({}, true, undefined, undefined,
      undefined, url).subscribe((halDocument: HalDocument<IllnessCollection>) => {

      halDocument.models.forEach(item => {
        illnesses.push((item as any).resource.illness);
      });
      const links = (halDocument as any).rawResource._links;

      if (links.next) {
        this.getNextPageIllnesses(this.illnesses, links.next.href);
      }
    });
  }

  getAllIllnesses() {
    if (!this.illnesses) {
      this.illnesses = [];
      this.getNextPageIllnesses(this.illnesses);
    }
    return this.illnesses;
  }
}
