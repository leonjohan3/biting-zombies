import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute} from '@angular/router';
import {HospitalService} from '../../services/hospital.service';
import {MatSort} from '@angular/material/sort';

interface SuggestedHospital {
  name: string;
  waitTime: number;
}

@Component({
  selector: 'app-hospitals',
  templateUrl: './hospitals.component.html',
  styleUrls: ['./hospitals.component.css']
})
export class HospitalsComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  showProgressBar = false;
  readonly displayedColumns = ['name', 'waitTime'];
  dataSource: MatTableDataSource<SuggestedHospital>;

  constructor(private activatedRoute: ActivatedRoute, private hospitalService: HospitalService) {
  }

  ngOnInit(): void {
    const painLevel = this.activatedRoute.snapshot.paramMap.get('painLevel');

    if (painLevel && +painLevel >= 0 && +painLevel <= 4) {
      this.showProgressBar = true;
      const suggestedHospitals: SuggestedHospital[] = [];

      this.hospitalService.getAllHospitals().forEach(hospital => {
        const waitingList = hospital.waitingList.find(item => item.levelOfPain === +painLevel);
        suggestedHospitals.push({name: hospital.name, waitTime: waitingList.patientCount * waitingList.averageProcessTime});
      });
      this.dataSource = new MatTableDataSource<SuggestedHospital>(suggestedHospitals);
      this.dataSource.sort = this.sort;

      this.sort.sort({
        disableClear: false, id: 'waitTime',
        start: 'asc'
      });
      this.showProgressBar = false;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
