import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Patient} from '../../model/patient';
import {PatientService} from '../../services/patient.service';
import {IllnessService} from '../../services/illness.service';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  showProgressBar = false;
  readonly displayedColumns = ['firstName', 'lastName', 'birthDate', 'painLevel', 'illness'];
  dataSource: MatTableDataSource<Patient>;
  readonly pageSizeOptions = [10, 20, 50];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private patientService: PatientService, private illnessService: IllnessService) {
  }

  ngOnInit(): void {
    this.showProgressBar = true;

    this.patientService.getAll().subscribe((patients: Patient[]) => {
      const newPatients: Patient[] = [];
      patients.forEach(patient => {
        const foundIllness = this.illnessService.getAllIllnesses().find(item => item.id === patient.illnessId);
        const illness = this.illnessService.getAllIllnesses().find(item => item.id === patient.illnessId).name;
        const newPatient = new Patient();
        newPatient.firstName = patient.firstName;
        newPatient.lastName = patient.lastName;
        newPatient.birthDate = patient.birthDate;
        newPatient.painLevel = patient.painLevel;
        newPatient.illness = illness;
        newPatients.push(newPatient);
      });
      this.dataSource = new MatTableDataSource<Patient>(newPatients);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.showProgressBar = false;

    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
