import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {PainLevelService} from '../../services/pain-level.service';
import {PainLevel} from '../../model/pain-level';
import {Illness} from '../../model/illness';
import {IllnessService} from '../../services/illness.service';
import {PatientService} from '../../services/patient.service';
import {Patient} from '../../model/patient';
import {Router} from '@angular/router';

@Component({
  selector: 'app-select-illness',
  templateUrl: './select-illness.component.html',
  styleUrls: ['./select-illness.component.css']
})
export class SelectIllnessComponent implements OnInit {

  startBirthDate: Date;
  minBirthDate: Date;
  maxBirthDate = new Date(Date.now());

  illnesses: Illness[];
  painLevels: PainLevel[];

  patientForm = this.formBuilder.group({
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    illnessId: [null, Validators.required],
    painLevel: [null, Validators.required],
    birthDate: [null, Validators.required]
  });

  constructor(private formBuilder: FormBuilder, private painLevelService: PainLevelService, private illnessService: IllnessService,
              private patientService: PatientService, private router: Router) {
    const currentYear = new Date().getFullYear();
    this.startBirthDate = new Date(currentYear - 30, 0, 1);
    this.minBirthDate = new Date(currentYear - 200, 0, 1);
  }

  ngOnInit(): void {
    this.painLevelService.getPainLevels().subscribe(painLevels => this.painLevels = painLevels);
    this.illnesses = this.illnessService.getAllIllnesses();
  }

  onSubmit() {

    this.patientService.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(this.patientForm.value.firstName, this.patientForm.value.lastName)
      .subscribe(patients => {

        if (patients.length > 0) {
          const patient = patients[0];
          patient.birthDate = new Date(this.patientForm.value.birthDate);
          patient.illnessId = this.patientForm.value.illnessId;
          patient.painLevel = this.patientForm.value.painLevel;
          this.patientService.update(patient).subscribe(updatedPatient => console.log('upd', updatedPatient));
        } else {
          const newPatient: Patient = new Patient();
          newPatient.firstName = this.patientForm.value.firstName;
          newPatient.lastName = this.patientForm.value.lastName;
          newPatient.birthDate = new Date(this.patientForm.value.birthDate);
          newPatient.illnessId = this.patientForm.value.illnessId;
          newPatient.painLevel = this.patientForm.value.painLevel;
          this.patientService.create(newPatient).subscribe(createdPatient => console.log('crt', createdPatient));
        }
      });
    this.router.navigate(['/suggested-hospitals', this.patientForm.value.painLevel]);
  }
}
