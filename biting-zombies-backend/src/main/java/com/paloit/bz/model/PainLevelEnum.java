package com.paloit.bz.model;

public enum PainLevelEnum {
    NONE, MILD, MODERATE, SEVERE, EXTREME
}
