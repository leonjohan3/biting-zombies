package com.paloit.bz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"first_name", "last_name"}))
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, nullable = false, updatable = false)
    private Integer id;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, name = "first_name")
    @NotBlank
    private String firstName;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, name = "last_name")
    @NotBlank
    private String lastName;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, name = "birth_date")
    @Past
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthDate;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, name = "illness_id")
    @Min(1)
    private Integer illnessId;

    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false, name = "pain_level")
    @NotNull
    private PainLevelEnum painLevel;
}
