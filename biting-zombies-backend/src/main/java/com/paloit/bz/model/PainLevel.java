package com.paloit.bz.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PainLevel {
    private int level;
    private String description;
}
