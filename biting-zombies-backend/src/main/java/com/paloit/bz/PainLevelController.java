package com.paloit.bz;

import com.paloit.bz.model.PainLevel;
import com.paloit.bz.model.PainLevelEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class PainLevelController {

    @GetMapping("/painLevels")
    public PainLevel[] getPainLevels() {
        return Arrays.stream(PainLevelEnum.values()).map(painLevelEnum -> new PainLevel(painLevelEnum.ordinal(), painLevelEnum.name()))
                .toArray(size -> new PainLevel[size]);
    }
}
