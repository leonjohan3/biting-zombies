package com.paloit.bz.repository;

import com.paloit.bz.model.Patient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PatientRepository extends CrudRepository<Patient, Integer> {
    List<Patient> findByFirstNameIgnoreCaseAndLastNameIgnoreCase(final String firstName, final String lastName);
}
