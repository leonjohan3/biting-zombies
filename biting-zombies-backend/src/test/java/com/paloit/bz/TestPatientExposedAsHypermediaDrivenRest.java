package com.paloit.bz;

import com.paloit.bz.model.PainLevelEnum;
import com.paloit.bz.model.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences.CollectionModelType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.h2.console.enabled=false",
                "spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_ON_EXIT=FALSE",
                "logging.level.root=warn"
        })
public class TestPatientExposedAsHypermediaDrivenRest {

    private static final String SERVICE_URI = "http://localhost:%s";
    private static final String PATIENT_REL = "patients";
    private static final String SELF_REL = "self";
    private static final String SEARCH_REL = "search";
    private static final String FIND_BY_REL = "findByFirstNameIgnoreCaseAndLastNameIgnoreCase";
    private static final int INITIAL_PATIENT_COUNT = 15;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private Traverson traversonClient;

    private String patientsLinkHref;

    @BeforeEach
    public void runThisOnceBeforeEachTest() {
        traversonClient = new Traverson(URI.create(String.format(SERVICE_URI, port)), MediaTypes.HAL_JSON);
        patientsLinkHref = traversonClient.follow(PATIENT_REL).asLink().getHref();
    }

    @Test
    public void shouldRetrieveAllPatients() {
        // given: all data (test fixture) preparation
        final CollectionModelType<Patient> collectionModelType = new CollectionModelType<Patient>() {
        };

        // when : method to be checked invocation
        final ResponseEntity<Object> responseEntity = traversonClient.follow(PATIENT_REL).toEntity(Object.class);
        final CollectionModel<Patient> patients = traversonClient.follow(PATIENT_REL).toObject(collectionModelType);

        // then : checks and assertions
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(patients.getContent(), hasSize(INITIAL_PATIENT_COUNT));
    }

    @Test
    public void shouldRetrieveSpecificPatient() {
        // given: all data (test fixture) preparation
        final CollectionModelType<EntityModel<Patient>> collectionModelType = new CollectionModelType<EntityModel<Patient>>() {
        };

        // when : method to be checked invocation
        final CollectionModel<EntityModel<Patient>> patients = traversonClient.follow(PATIENT_REL).toObject(collectionModelType);
        final EntityModel<Patient> patientModel = patients.iterator().next();
        final ResponseEntity<Patient> patientEntity = restTemplate.getForEntity(patientModel.getLink(SELF_REL).get().getHref(), Patient.class);

        // then : checks and assertions
        assertThat(patients.getContent(), hasSize(INITIAL_PATIENT_COUNT));
        assertThat(patientEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(patientEntity.getBody().getFirstName(), not(blankOrNullString()));
    }

    @Test
    public void shouldCreateNewPatientAndThenDelete() {
        // given: all data (test fixture) preparation
        final Patient patient = new Patient();
        patient.setFirstName("FirstName");
        patient.setLastName("Last Name");
//        patient.setBirthDate(new Date());
        patient.setBirthDate(LocalDate.now().minusYears(20));
        patient.setIllnessId(3);
        patient.setPainLevel(PainLevelEnum.MILD);

        final CollectionModelType<Patient> collectionModelType = new CollectionModelType<Patient>() {
        };

        // when : method to be checked invocation
        final ResponseEntity<Patient> newPatient = restTemplate.postForEntity(patientsLinkHref, patient, Patient.class);
        CollectionModel<Patient> patients = traversonClient.follow(PATIENT_REL).toObject(collectionModelType);

        // then : checks and assertions
        assertThat(patients.getContent(), hasSize(INITIAL_PATIENT_COUNT + 1));
        assertThat(newPatient.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(newPatient.getBody(), is(patient));

        // new patient created above, now search for and delete the patient
        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstName", patient.getFirstName());
        parameters.put("lastName", patient.getLastName());

        // when : method to be checked invocation
        patients = traversonClient.follow(PATIENT_REL, SEARCH_REL, FIND_BY_REL).withTemplateParameters(parameters)
                .toObject(collectionModelType);

        // then : checks and assertions
        assertThat(patients.getContent(), hasSize(1));
        assertThat(patients.getContent().iterator().next().getLastName(), is(patient.getLastName()));

        // when : method to be checked invocation
        restTemplate.delete(newPatient.getHeaders().getLocation());
        patients = traversonClient.follow(PATIENT_REL).toObject(collectionModelType);

        // then : checks and assertions
        assertThat(patients.getContent(), hasSize(INITIAL_PATIENT_COUNT));

        // when : method to be checked invocation
        patients = traversonClient.follow(PATIENT_REL, SEARCH_REL, FIND_BY_REL).withTemplateParameters(parameters)
                .toObject(collectionModelType);

        // then : checks and assertions
        assertThat(patients.getContent(), hasSize(0));
    }

    private EntityModel<Patient> getFirstPatient() {
        final CollectionModelType<EntityModel<Patient>> collectionModelType = new CollectionModelType<EntityModel<Patient>>() {
        };
        final CollectionModel<EntityModel<Patient>> patients = traversonClient.follow(PATIENT_REL).toObject(collectionModelType);
        return patients.iterator().next();
    }

    @Test
    public void shouldUpdateExistingPatient() {
        // given: all data (test fixture) preparation
        final EntityModel<Patient> patientModel = getFirstPatient();
        final Patient existingPatient = patientModel.getContent();
        final String newFirstName = new StringBuilder(existingPatient.getFirstName()).reverse().toString();
        existingPatient.setFirstName(newFirstName);

        // when : method to be checked invocation
        final ResponseEntity<Patient> updatedPatientEntity = restTemplate.exchange(patientModel.getLink(SELF_REL).get().getHref(),
                HttpMethod.PATCH, new HttpEntity<Patient>(existingPatient), Patient.class);

        // then : checks and assertions
        assertThat(updatedPatientEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(updatedPatientEntity.getBody().getFirstName(), is(newFirstName));
    }

    @Test
    public void shouldValidateUniqueConstraintForFirstNameLastNameAndBirthDate() {
        // given: all data (test fixture) preparation
        final EntityModel<Patient> patientModel = getFirstPatient();
        final Patient patient = restTemplate.getForObject(patientModel.getLink(SELF_REL).get().getHref(), Patient.class);
        final Patient similarPatient = new Patient(null, patient.getFirstName(), patient.getLastName(), patient.getBirthDate().plusDays(1),
                1, PainLevelEnum.NONE);

        // when : method to be checked invocation
        final ResponseEntity<Patient> newPatient = restTemplate.postForEntity(patientsLinkHref, similarPatient, Patient.class);

        // then : checks and assertions
        assertThat(newPatient.getStatusCode(), is(HttpStatus.CONFLICT));
    }

    @Test
    public void shouldValidateFirstNameNotAsOnlySpaces() {
        // given: all data (test fixture) preparation
        final EntityModel<Patient> patientModel = getFirstPatient();
        final Patient patient = restTemplate.getForObject(patientModel.getLink(SELF_REL).get().getHref(), Patient.class);
        patient.setFirstName("  ");

        // when : method to be checked invocation
        final ResponseEntity<Patient> updatedPatientEntity = restTemplate.exchange(patientModel.getLink(SELF_REL).get().getHref(),
                HttpMethod.PATCH, new HttpEntity<Patient>(patient), Patient.class);

        // then : checks and assertions
        assertThat(updatedPatientEntity.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @Test
    public void shouldValidateBirthDateNotInTheFuture() {
        // given: all data (test fixture) preparation
        final EntityModel<Patient> patientModel = getFirstPatient();
        final Patient patient = restTemplate.getForObject(patientModel.getLink(SELF_REL).get().getHref(), Patient.class);
        patient.setBirthDate(LocalDate.now().plusDays(1));

        // when : method to be checked invocation
        final ResponseEntity<Patient> updatedPatientEntity = restTemplate.exchange(patientModel.getLink(SELF_REL).get().getHref(),
                HttpMethod.PATCH, new HttpEntity<Patient>(patient), Patient.class);

        // then : checks and assertions
        assertThat(updatedPatientEntity.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));
    }
}
