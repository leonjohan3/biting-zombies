# Overview
This project contains the Palo IT coding challenge. The functionality is based on the specification [here](</Palo_coding_challenge.pdf>). 
The application is deployed [here](<http://consolidate.mooo.com:8161>).

# System requirements
## Backend
* Java JDK 1.8 or later
* Maven 3.2+
* TCP port 8080 available
* If you need to work with this code in your IDE, you will need to first configure https://projectlombok.org/setup/overview

## Front-end
* Nodejs 10+
* npm
* Install Angular, https://angular.io/guide/setup-local

# Running the backend
* After cloning this repository, change directory to `biting-zombies/biting-zombies-backend` and then run `mvn test spring-boot:run`
* The above command will both run the tests and start the application.
* Database files will be created in the current folder.
* HAL Explorer is here http://localhost:8080
* To view the contents of the H2 embedded database, login to the http://localhost:8080/h2-console and run `select * from patient` (see image below for the connection details)
![h2-console](/h2-login-console.png "H2 Console")

# Running the front-end
* Change directory to `biting-zombies/biting-zombies-frontend`
* Run `npm i`
* Run `ng s`
* Point browser to `http://localhost:4200`
* A side menu shows two options.

# Backend Resources
* [Spring Boot](<https://docs.spring.io/spring-boot/docs/2.2.9.RELEASE/reference/html/index.html>)
* [Spring Data REST](<https://docs.spring.io/spring-data/rest/docs/3.2.9.RELEASE/reference/html/#reference>)
* [Spring Hateoas](<https://docs.spring.io/spring-hateoas/docs/1.0.5.RELEASE/reference/html/#client>)
* [Initialize a Database using basic SQL scripts](<https://docs.spring.io/spring-boot/docs/2.2.9.RELEASE/reference/html/howto.html#howto-initialize-a-database-using-spring-jdbc>)

# Front-end Resources
* [Angular](<https://angular.io/docs>)
* [Angular Material](<https://material.angular.io>)
* [Angular Flex-Layout](<https://github.com/angular/flex-layout/>)
